/**
 * 
 */
$(document).ready(function() {
    

    $(window).load(function(){
        setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
    });
    
    $('tr').mouseover(function() {
        button = $(this).find('.edit-option');
        button.css('display', 'block');
    });
    $('tr').mouseout(function() {
        button = $(this).find('.edit-option');
        button.css('display', 'none');
    });
    
});